import {defineNuxtConfig} from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    buildModules: ['@nuxtjs/tailwindcss'],
    publicRuntimeConfig: {
        BASE_URL: process.env.BASE_URL || 'http://localhost:8080',
        PLUGIN_URL: process.env.BASE_URL || 'http://localhost:8080',
        DATA_URL: process.env.DATA_URL || 'http://localhost:8080'
    }
})

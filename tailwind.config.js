const colors = require('tailwindcss/colors')
const typography = require('@tailwindcss/typography')

module.exports = {
    plugins: [
        typography
    ],
    theme: {
        colors: {
            ...colors,
            'gray-1': '#141414',
            'gray-2': '#282828',
            'primary': '#B10CE2',
            'primary-smooth': 'rgba(177,12,226,0.05)',
        },
    }
}